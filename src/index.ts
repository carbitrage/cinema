import TelegramBot, { Message } from 'node-telegram-bot-api';
import { config as dotenvConfig } from 'dotenv';
import { createUserIfNotExists } from './db';

dotenvConfig();
const token: string = process.env.TG_TOKEN!;
const bot: TelegramBot = new TelegramBot(token, {polling: true});

bot.onText(/\/start/, (msg: Message) => {
    const chatId: number = msg.chat.id;
    createUserIfNotExists(chatId);
    const resp: string = msg.chat.id.toString();
    bot.sendMessage(chatId, `Your User ID: ${resp}`);
  });