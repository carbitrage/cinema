import { Client, QueryResult } from "pg";
import 'dotenv';

function createUserIfNotExists(uid: number) {
    const client: Client = new Client({
        user: process.env.DB_USER, 
        host: process.env.DB_HOST, 
        port: +process.env.DB_PORT!, 
        database: process.env.DB_NAME, 
        password: process.env.DB_PASSWORD
    });
    client.connect();
    client.query(`SELECT uid FROM users WHERE uid = '${uid.toString()}';`)
        .then((response: QueryResult) => {
            if (response.rowCount > 0) {
                client.end();
            } else {
                client.query(`INSERT INTO users (uid) VALUES (${uid});`)
                    .then(() => {
                        client.end();
                    })
                    .catch((error: Error) => {
                        console.error(`Database ${error.stack}`);
                    });
            }
        })
        .catch((error: Error) => {
            console.error(`Database ${error.stack}`);
        });
}

export { createUserIfNotExists };